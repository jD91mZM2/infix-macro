(load "infix-macro.lisp")

(defun check (actual expected)
  (let ((parsed (parse-infix (cdr actual)))
        (expanded (macroexpand-1 actual)))
    (format t "Assert ~A = ~A~%" actual expected)
    (assert (equal parsed expected))
    (assert (equal expanded expected))
    (assert (equal (eval actual) (eval expected)))))

(check '(math 1 + 2) '(+ 1 2))
(check '(math 1 * 2) '(* 1 2))
(check '(math 1 * 2 + 3) '(+ (* 1 2) 3))
(check '(math 1 * 2 + 3 / 4) '(+ (* 1 2) (/ 3 4)))
(check '(math 1 * 2 + 3 / 4 - 5) '(- (+ (* 1 2) (/ 3 4)) 5))
(check '(math 1 + 2 + 3 + 4 * 5) '(+ (+ (+ 1 2) 3) (* 4 5)))

(format t "All asserts worked! Magic!~%")
