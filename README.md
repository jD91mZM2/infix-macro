# Lisp infix-macro

I'm just playing around with lisp, ignore me. I like testing
boundaries and breaking things, but still, apologies to you hardcore
lisp fans out there if I broke your lisp. I don't regret it.

## Usage

```lisp
(print (math 1 + 2)) ;; => (print (+ 1 2))
(print (math 1 * 2)) ;; => (print (* 1 2))
(print (math 1 * 2 + 3)) ;; => (print (+ (* 1 2) 3))
(print (math 1 * 2 + 3 / 4)) ;; => (print (+ (* 1 2) (/ 3 4)))
(print (math 1 * 2 + 3 / 4 - 5)) ;; => (print (- (+ (* 1 2) (/ 3 4)) 5))
```

Any questions? Feel free to ask `/dev/zero` for an explanation.
