(defstruct (operator (:conc-name op-))
  priority infix prefix)

(defparameter *operators* (list (make-operator :priority 1 :infix '+ :prefix '+)
                                (make-operator :priority 1 :infix '- :prefix '-)
                                (make-operator :priority 2 :infix '* :prefix '*)
                                (make-operator :priority 2 :infix '/ :prefix '/)
                                (make-operator :priority 2 :infix '% :prefix 'rem)))

(defstruct (parsed)
  remaining result)

(defun parse-infix-inner (priority input)
  ;; Check if the highest priority has been exceeded
  (let ((max (loop for operator in *operators*
                maximize (op-priority operator))))
    (if (> priority max)
        ;; If yes, there are no more infix operations to
        ;; perform. Is this a list (nested expression, that is)?
        (if (typep (car input) 'list)
            ;; Yep, it's a list. Handle any inner math!
            (make-parsed :remaining (cdr input) :result (parse-infix (car input)))
            ;; Not a list, so return the raw node in the AST (probably a number).
            (make-parsed :remaining (cdr input) :result (car input)))

        ;; Otherwise:

        ;; Consume nested expression as result
        (let* ((nested (parse-infix-inner (1+ priority) input))
               (result (parsed-result nested))
               (matching t))
          (setq input (parsed-remaining nested))

          ;; While list isn't empty and an operator previously matched
          (loop while (and input matching)
             do (setq matching nil)

             ;; For each operator of the same precedence
             do (loop for operator in *operators*
                   do (when (and (equal (op-priority operator) priority)
                                 (equal (op-infix operator) (car input)))
                        (setq matching t)

                        ;; Consume operator
                        (setq input (cdr input))

                        ;; Consume second number and modify result
                        (let ((nested (parse-infix-inner (1+ priority) input)))
                          (setq result (list (op-prefix operator) result (parsed-result nested)))
                          (setq input (parsed-remaining nested))))))

          ;; Return remaining input and result
          (make-parsed :remaining input :result result)))))

(defun parse-infix (input)
  (let ((returned (parse-infix-inner 1 input)))
    (when (parsed-remaining returned)
      (format t "Warning: The following tokens could not be parsed: ~A~%" (parsed-remaining returned)))
    (parsed-result returned)))

(defmacro math (&rest input)
  (parse-infix input))
